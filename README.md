# PraLicia

Projet réaliser pour ma conjointe

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
npm install
docker-compose up -d
symfony serve -d
npm run build
```

### Ajouter des données de tests

```bash
symfony console doctrine:fixture:load
```

## Lancer des tests

```bash
php bin/phpunit --testdox
```