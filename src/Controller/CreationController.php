<?php

namespace App\Controller;

use App\Repository\CreationRepository;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreationController extends AbstractController
{
    /**
     * @Route("/realisations", name="realisations")
     */
    public function realisations(
        CreationRepository $creationRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $creationRepository->findAll();

        $creations = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );
        return $this->render('realisations/realisations.html.twig', [
            'creations' => $creations,
        ]);
    }
}
