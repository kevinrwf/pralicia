<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\CreationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(CreationRepository $creationRepository, BlogpostRepository $blogpostRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'creations' => $creationRepository->lastThree(),
            'blogposts' => $blogpostRepository->lastThree(),
        ]);
    }
}
