<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Creation;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        // utilisation de Faker
        $faker = Factory::create('fr_FR');

        //realisations d'un utilisateur
        $user = new User();

        $user->setEmail('user@test.com')
            ->setPrenom($faker->firstName())
            ->setNom($faker->lastName())
            ->setAPropos($faker->text())
            ->setInstagram('instagram')
            ->setRoles(['ROLE_CREATEUR']);

        $password = $this->encoder->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        // creaction de 10 blogpost
        for ($i=0; $i <10; $i++) {
            $blogpost = new Blogpost();

            $blogpost->setTitre($faker->words(3, true))
                    ->setDate($faker->dateTimeBetween('-6 month', 'now'))
                    ->setContenu($faker->text(350))
                    ->setSlug($faker->slug(3))
                    ->setUser($user);

            $manager->persist($blogpost);
        }

        // realisations de 5 catégories
        for ($k=0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->word())
                    ->setDescription($faker->words(10, true))
                    ->setSlug($faker->slug());

            $manager->persist($categorie);

            // realisations de 2 "Creation" par categories
            for ($j=0; $j < 2; $j++) {
                $creation = new Creation();

                $creation->setNom($faker->words(3, true))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setDate($faker->dateTimeBetween('-6 month', 'now'))
                    ->setPrix($faker->randomFloat(2, 100, 999))
                    ->setDescription($faker->words(10, true))
                    ->setPortfolio($faker->randomElement([true, false]))
                    ->setSlug($faker->slug())
                    ->setFile('/img/placeholder.jpg')
                    ->addCategorie($categorie)
                    ->setUser($user);

                $manager->persist($creation);
            }
        }

        $manager->flush();
    }
}
