<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testBlogpostIsTrue(){

        $blogpost = new Blogpost();
        $datetime = new \DateTime();
        $user = new User();

        $blogpost->setTitre('titre')
            ->setDate($datetime)
            ->setContenu('contenu')
            ->setSlug('slug')
            ->setUser($user);

        $this->assertTrue($blogpost->getTitre() === 'titre');
        $this->assertTrue($blogpost->getDate() === $datetime);
        $this->assertTrue($blogpost->getContenu() === 'contenu');
        $this->assertTrue($blogpost->getSlug() === 'slug');
        $this->assertTrue($blogpost->getUser() === $user);
    }

    public function testBlogpostIsFalse(){

        $blogpost = new Blogpost();
        $datetime = new \DateTime();
        $user = new User();

        $blogpost->setTitre('titre')
            ->setDate($datetime)
            ->setContenu('contenu')
            ->setSlug('slug')
            ->setUser($user);

        $this->assertFalse($blogpost->getTitre() === 'false');
        $this->assertFalse($blogpost->getDate() === new \DateTime());
        $this->assertFalse($blogpost->getContenu() === 'false');
        $this->assertFalse($blogpost->getSlug() === 'false');
        $this->assertFalse($blogpost->getUser() === new User());
    }
}
