<?php

namespace App\Tests;

use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testCategorieIsTrue() {

        $categorie = new Categorie();

        $categorie->setNom('nom')
                  ->setDescription('description')
                  ->setSlug('slug');

        $this->assertTrue($categorie->getNom() === 'nom');
        $this->assertTrue($categorie->getDescription() === 'description');
        $this->assertTrue($categorie->getSlug() === 'slug');
    }

    public function testCategorieIsFalse() {

        $categorie = new Categorie();

        $categorie->setNom('nom')
                  ->setDescription('description')
                  ->setSlug('slug');

        $this->assertFalse($categorie->getNom() === 'false');
        $this->assertFalse($categorie->getDescription() === 'false');
        $this->assertFalse($categorie->getSlug() === 'false');
    }
}
