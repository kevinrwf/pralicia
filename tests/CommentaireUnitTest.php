<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Creation;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testCommentaireIsTrue(){

        $commentaire = new Commentaire();
        $datetime = new \DateTime();
        $blogpost = new Blogpost();
        $creation = new Creation();

        $commentaire->setAuteur('auteur')
            ->setEmail('email@test.com')
            ->setDate($datetime)
            ->setContenu('contenu')
            ->setBlogpost($blogpost)
            ->setCreation($creation);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getDate() === $datetime);
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getCreation() === $creation);
    }

    public function testCommentaireIsFalse(){

        $commentaire = new Commentaire();
        $datetime = new \DateTime();
        $blogpost = new Blogpost();
        $creation = new Creation();

        $commentaire->setAuteur('auteur')
            ->setEmail('email@test.com')
            ->setDate($datetime)
            ->setContenu('contenu')
            ->setBlogpost($blogpost)
            ->setCreation($creation);

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false@test.com');
        $this->assertFalse($commentaire->getDate() === new \DateTime());
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
        $this->assertFalse($commentaire->getCreation() === new Creation());
    }
}
