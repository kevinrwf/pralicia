<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Creation;
use App\Entity\User;
use PHPUnit\Framework\TestCase;


class CreationUnitTest extends TestCase
{
    public function testCreationIsTrue(){

        $creation = new Creation();
        $datetime = new \DateTime();
        $categorie = new Categorie();
        $user = new User();

        $creation->setNom('nom')
            ->setDescription('description')
            ->setEnVente(true)
            ->setPortfolio(true)
            ->setDate($datetime)
            ->setPrix(20.20)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setUser($user);

        $this->assertTrue($creation->getNom() === 'nom');
        $this->assertTrue($creation->getDescription() === 'description');
        $this->assertTrue($creation->getEnVente() === true);
        $this->assertTrue($creation->getPortfolio() === true);
        $this->assertTrue($creation->getDate() === $datetime);
        $this->assertTrue($creation->getPrix() == 20.20);
        $this->assertTrue($creation->getSlug() === 'slug');
        $this->assertTrue($creation->getFile() === 'file');
        $this->assertContains($categorie, $creation->getCategorie());
        $this->assertTrue($creation->getUser() === $user);
    }

    public function testCreationIsFalse(){

        $creation = new Creation();
        $datetime = new \DateTime();
        $categorie = new Categorie();
        $user = new User();

        $creation->setNom('nom')
            ->setDescription('description')
            ->setEnVente(true)
            ->setPortfolio(true)
            ->setDate($datetime)
            ->setPrix(20.20)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setUser($user);

        $this->assertFalse($creation->getNom() === 'false');
        $this->assertFalse($creation->getDescription() === 'false');
        $this->assertFalse($creation->getEnVente() === false);
        $this->assertFalse($creation->getPortfolio() === false);
        $this->assertFalse($creation->getDate() === new \DateTime());
        $this->assertFalse($creation->getPrix() == 21.21);
        $this->assertFalse($creation->getSlug() === 'false');
        $this->assertFalse($creation->getFile() === 'false');
        $this->assertNotContains(new Categorie(), $creation->getCategorie());
        $this->assertFalse($creation->getUser() === new User());
    }
}
